#!/usr/bin/env ruby

LSB = {}
IO.read("/etc/lsb-release").split("\n").each do |x|
	y = x.split("=")
	LSB[y[0].to_sym] = (y[1][0] == "\"") ? eval(y[1]) : y[1]
end

puts LSB[:DISTRIB_DESCRIPTION]
