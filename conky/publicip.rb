#!/usr/bin/env ruby

require 'net/http'

data = Net::HTTP.get(URI.parse("http://checkip.dyndns.org"))
ipaddr = /(\d+.\d+.\d+.\d+)/.match(data)
puts ipaddr[1]
