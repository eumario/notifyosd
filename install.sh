#!/bin/bash
echo modified notifyOSD Conky installation script.
echo This script will backup your current conky, if it exists,
echo and install the modified notifyOSD Conky configuration.
echo Please wait...

if [[ -e ~/.conkyrc ]]; then
	mv ~/.conkyrc ~/.conkyrc.backup.mnotifyOSD
fi

if [[ -e ~/.conky ]]; then
	mv ~/.conky ~/.conky.backup.mnotifyOSD
fi

echo Installing....

cp conkyrc ~/.conkyrc
cp -R conky ~/.conky

echo Installation complete, if a .conkyrc file existed
echo before installation, it has been moved to .conkyrc.backup.mnotifyOSD
echo
echo If a .conky folder existed before installation, it has
echo been moved to .conky.backup.mnotifyOSD
echo
echo Installation completed.
